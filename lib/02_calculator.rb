def add(num1, num2)
  num1 + num2
end

def subtract(num, subtractor)
  num - subtractor
end

def sum(arr)
  arr.inject(0, :+)
end

def multiply(*nums)
  product = 1
  nums.each {|num| product *= num}
  product
end

def power(num, power_of)
  num ** power_of
end

def factorial(num)
  factor = 1
  while (num - 1) > 0
    factor *= num
    num -= 1
  end
  factor
end
