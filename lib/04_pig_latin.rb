def translate(str)
  pig_words = str.split(' ').map do |word|
    n = remove_n_letters(word)
    word = word[n..-1] + word[0...n] + "ay"
  end
  pig_words.join(' ')
end

def remove_n_letters(word)
  vowels = %w(a e i o u)
  i = 0
  if vowels.include?(word[0])
    return i
  else
    until vowels.include?(word[i]) || i == word.length
      i += 1
    end
    if word[i-1] == "q" && word[i] == "u"
      i += 1
    end
  end
  i
end
