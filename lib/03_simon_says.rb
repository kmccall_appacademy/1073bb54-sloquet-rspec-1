def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(str, n = 2)
  [str] * n * ' '
end

def start_of_word(str, num)
  str[0...num]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  words = str.split
  words[0].capitalize!
  little_words = ["over", "the", "and"]
  fixed_words = words.map do |word|
    if little_words.include?(word)
      word
    else
      word.capitalize
    end
  end
  fixed_words.join(' ')
end
